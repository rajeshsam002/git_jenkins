terraform {
  backend "s3" {
    bucket = "s3-state-jenkins"
    key    = "terraform.tfstate"
    region = "ap-south-1"
  }
}