FROM tomcat
COPY ./target/dante.war /usr/local/tomcat/webapps/ROOT.war
CMD ["catalina.sh", "run"]